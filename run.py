import os
import torch
import torchvision
import pickle
import numpy as np

from PIL import Image
from robic import RoBIC



device = 0
output_folder = "./results"
# Get_model
model = torchvision.models.resnet50(pretrained=True).eval()

# Get Data
X, y = [], []
for image in os.listdir("images"):
    label = int(image[:-4].split("_")[-1])
    img = Image.open(os.path.join("images", image))
    X.append(np.array(img).transpose(2, 0, 1))
    y.append(label)

X = torch.Tensor(X)
y = torch.Tensor(y)

if device != None and device != "cpu":
    model = model.cuda(device)
    X = X.cuda(device)
    y = y.cuda(device)

# Benchmark

robic = RoBIC(device=device, output_folder=output_folder)
accuracy, wb_robustness, bb_robustness = robic.run(model, X, y, save=True)
print("Accuracy: {:2f}".format(accuracy))
print("White Box Half-Disortion: {:2f}".format(wb_robustness))
print("Black Box Half-Disortion: {:2f}".format(bb_robustness))


