# RoBIC: A BENCHMARK SUITE FOR ASSESSING CLASSIFIERS ROBUSTNESS

![Robustness Plot](./results/robustness.png)

Many defenses have emerged with the development of adversarial attacks. Models must be objectively evaluated accordingly. This paper systematically tackles this concern by proposing a new parameter-free benchmark we coin RoBIC. RoBIC fairly evaluates the robustness of image classifiers using a new half-distortion measure. It gauges the robustness of the network against white and black box attacks, independently of its accuracy. RoBIC is faster than the other available benchmarks. We present the significant differences in the robustness of 16 recent models as assessed by RoBIC. 

Paper Available at: https://arxiv.org/abs/2102.05368 

# Install

Install Requirements
```bash
pip install -r requirements
```

Install Package
```bash
python setup.py install
```

# Run

Benchmark your [Pytorch](https://pytorch.org/) models
RoBIC is really easy to use.  
Here is an example. You can find it in *run.py*.

* Load Image

```python
import os
import torch
import numpy as np

from PIL import Image

X, y = [], []
for image in os.listdir("images"):
    label = int(image[:-4].split("_")[-1])
    img = Image.open(os.path.join("images", image))
    X.append(np.array(X).transpose(2, 0, 1))
    y.append(label)

X = torch.Tensor(X)
y = torch.Tensor(y)
```

* Load your model. Here you choose resnet50 from torchvision.

```python
import torchvision
model = torchvision.models.resnet50(pretrained=True)
model = model.eval()
```

* Now your model can be easily benchmarked:

from robic.benchmark import RoBIC
```python
from robic import RoBIC
bench = RoBIC()
accuracy, wb_robustness, bb_robustness = bench.run(model, X, y)
```

# Results

Here is the benchmark.
The half-disortion are obtained with 1.000 ImageNet Images.

| Model| Parameters | Accuracy  | HD White-Box | HD Black-Box |
|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|[AlexNet](http://arxiv.org/abs/1404.5997) | 62.38 | 56.8 | 0.19 | 2.17|
|[CSPResNeXt50](https://arxiv.org/abs/1911.11929) | 20.57 | 84.6 | 0.13 | 4.48 |
|[DualPathNetworks 68b](https://arxiv.org/abs/1707.01629) | 12.61 | 83.8 | 0.08 | 3.82 |
|[MixNet Large](https://arxiv.org/abs/1907.09595) | 7.33 | 84.2 | 0.12 | 2.96 |
|[MobileNetV2](https://arxiv.org/abs/1801.04381) | 5.83 | 80.1 | 0.09 | 2.90 |
|[ReXNet 200](https://arxiv.org/abs/2007.00992) | 16.37 | 85.4 | 0.14 | 3.89 |
|[RegNetY 032](https://arxiv.org/abs/2003.13678) | 19.44 | 85.8 | 0.11 | 4.94 |
|[SEResNeXt50 32x4d](https://arxiv.org/abs/1709.01507) | 27.56 |  85.9 | 0.12 |  5.01 |
|[VGG16](http://arxiv.org/abs/1409.1556) | 138.00 | 74.9 | 0.09 | 2.44 |
|[EfficientNet AdvProp](https://arxiv.org/abs/1911.09665) | 5.29 | 84.3 |  0.31 | 4.35 |
|[EfficientNet EdgeTPU Small](http://proceedings.mlr.press/v97/tan19a.html) | 5.44 | 82.8 | 0.15 | 3.16 |
|[EfficientNet NoisyStudent](https://arxiv.org/abs/1911.04252) | 5.29 | 82.7 | 0.19 | 2.37 |
|[EfficientNet](http://proceedings.mlr.press/v97/tan19a.html) | 5.29 | 82.8 | 0.17 | 3.56 |
|[ResNet50](https://arxiv.org/abs/1512.03385) ([torchvision](https://doi.org/10.1145/1873951.1874254))  | 25.56 | 77.9 | 0.10 | 2.77 |
|[ResNet50](https://arxiv.org/abs/1512.03385) ([timm](https://doi.org/10.5281/zenodo.4414861))  | 25.56 | 80.5 | 0.15 | 4.35 |
|[ResNet50 AdvTrain](https://openreview.net/forum?id=rJzIBfZAb) | 25.56 | 60.8 |  2.56 |  9.88 |
|[ViT Base Patch=16](https://arxiv.org/abs/2010.11929) | 81.79 | 86.6 | 0.24 | 1.93 |
|[DeiT Distilled Base Patch=16](https://arxiv.org/abs/2012.12877) | 87.34 | 83.388 | 0.3 | 3.29 | 



# Citation

```
@misc{maho2021robic,
      title={RoBIC: A benchmark suite for assessing classifiers robustness}, 
      author={Thibault Maho and Benoît Bonnet and Teddy Furon and Erwan Le Merrer},
      year={2021},
      eprint={2102.05368},
      archivePrefix={arXiv},
      primaryClass={cs.CV}
}
```
