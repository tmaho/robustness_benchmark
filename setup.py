from distutils.core import setup

setup(name='robic',
      version='1.0',
      description='Robustness Benchmark for Image Classifier',
      author='Thibault Maho, Benoit Bonnet, Teddy Furon, Erwan Le Merrer',
      author_email='thibault.maho@inria.fr',
      url=''
     )