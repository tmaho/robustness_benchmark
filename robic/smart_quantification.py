import torch
import numpy as np


# Former quantizer.max_dist
max_dist = 1
# Former quantizer.binary_search_steps
binary_search_steps = 10
# Former quantizer.pytorch_device



def best_quantization_variance(grads, model, adversarial_images, images_orig, init_labels):
    """
    This is the main function for the  steganographic quantization with the quadratic method.
    adversarial_images = Tensor of the already attacked images
    images_orig = Tensor of the original images before attack
    init_labels = Ground Truth labels for images
    """
    batch_size = len(adversarial_images)
    perturbations_adv = adversarial_images - images_orig

    loss = classif_loss(model, init_labels, images_orig)
    already_adv = (loss<=0).float()

    loss_th_base =  classif_loss(model, init_labels, adversarial_images)
    perturbation_term = (grads*(adversarial_images-images_orig)).view(batch_size,-1).sum(axis=1)
    lambdas_critic = find_lambdas_critic(grads, loss_th_base-perturbation_term)

    quantization_mat = quantize_lambda(perturbations_adv, grads, 100*lambdas_critic, device=model.device)
    adversarial = torch.clamp(quantization_mat+images_orig, 0, 255)
    best_adversarial = adversarial

    lower_lambda = torch.zeros(batch_size, device=model.device)
    upper_lambda = 100*lambdas_critic
    lambda_search = (upper_lambda+lower_lambda)/2

    for _ in range(binary_search_steps):
        quantization_mat = quantize_lambda(perturbations_adv, grads, lambda_search, device=model.device)
        adversarial = torch.clamp(quantization_mat+images_orig, 0, 255)
        loss = classif_loss(model, init_labels, adversarial)

        best_adversarial = (loss<=0).float().view(batch_size,1,1,1)*adversarial + (loss>0).float().view(batch_size,1,1,1)*best_adversarial

        lower_lambda = (loss>0).float()*lambda_search + (loss<=0).float()*lower_lambda
        upper_lambda = (loss<=0).float()*lambda_search + (loss>0).float()*upper_lambda
        lambda_search = (lower_lambda+upper_lambda)/2
    best_adversarial = already_adv.view(batch_size,1,1,1)*images_orig + (1-already_adv).view(batch_size,1,1,1)*best_adversarial
    return(best_adversarial.detach())


def find_lambdas_critic(gradients, loss_th_0):
    """
    This function calculates all the possibles values of Lambda for which the quantization of a pixel
    will swap from minimum distortion to maximum distortion
    """
    batch_size = gradients.shape[0]
    grad_norm_squared = (gradients.view(batch_size,-1).norm(dim=1))**2

    lambda_critic = 2*(loss_th_0)/grad_norm_squared
    lambda_critic = torch.max(lambda_critic, torch.ones(lambda_critic.shape, device=lambda_critic.device))

    return(lambda_critic)

def quantize_lambda(perturbations, gradients, lambda_val, device):
    """
    This function returns the quantized perturbation for a given value of lambda.
    """
    batch_size = perturbations.shape[0]

    #Applying the quantization method and keeping it inside the maximum distortion boundaries
    quantized = -lambda_val.view(batch_size,1,1,1)*gradients/2
    quantized = quantized.round().to(device)
    #quantized_bounded = torch.clamp(quantized,-maximum_distortion, maximum_distortion)

    if max_dist%2==0:
        max_pert = (perturbations.round() + max_dist/2)
        min_pert = (perturbations.round() - max_dist/2)
    else:
        max_pert = (perturbations.ceil() + (max_dist-1)/2)
        min_pert = (perturbations.floor() - (max_dist-1)/2)

    quantized_bounded_min = torch.min(max_pert.int().float(), quantized)
    quantized_bounded = torch.max(min_pert.int().float(), quantized_bounded_min)

    #quantized_bounded = torch.max(quantized,perturbations.floor() - maximum_distortion-1)
    #quantized_bounded = torch.min(quantized_bounded,perturbations.ceil() + maximum_distortion-1)
    #This ensures that the quantization doesn't go backward and cancels the perturbation
    quantized_bounded[perturbations>0] = torch.max((quantized_bounded[perturbations>0]), torch.zeros(perturbations[perturbations>0].shape).to(device))
    quantized_bounded[perturbations<0] = torch.min((quantized_bounded[perturbations<0]), torch.zeros(perturbations[perturbations<0].shape).to(device))
    return(quantized_bounded)

def classif_loss(model, init_label, py_image):
    """
    The loss used to study adverariality. If <0, then the image is adversarial
    """
    prediction = model(py_image / 255)
    init_labels_onehot = torch.zeros(init_label.size(0), 1000, device=model.device)
    init_labels_onehot.scatter_(1, init_label.unsqueeze(1).long(),1)

    labels = prediction.argsort(1)
    adv_class = labels[:,-1]
    adv_class = (adv_class==init_label)*labels[:,-2]+(adv_class!=init_label)*adv_class

    adv_labels_onehot = torch.zeros(adv_class.size(0), 1000, device=model.device)
    adv_labels_onehot.scatter_(1, adv_class.unsqueeze(1).long(),1)

    classification_loss = prediction*init_labels_onehot-prediction*adv_labels_onehot
    classification_loss = classification_loss.sum(axis=1)

    return(classification_loss)