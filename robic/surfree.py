import logging
import numpy as np
import torch
import copy
import random
import eagerpy as ep
import math

from scipy import fft

from foolbox.models import Model

from foolbox.criteria import Criterion

from foolbox.distances import l2

from foolbox.devutils import atleast_kd

from foolbox.attacks.blended_noise import LinearSearchBlendedUniformNoiseAttack

from foolbox.attacks.base import MinimizationAttack, get_criterion, get_is_adversarial
from foolbox.attacks.base import T

from typing import Callable, Union, Optional, Tuple, List, Any, Dict


class SurFree(MinimizationAttack):
    distance = l2
    def __init__(
        self, 
        steps: int = 100, 
        max_queries: int = 5000,
        BS_gamma: float = 0.05, 
        BS_max_iteration: int = 7, 
        theta_max: float = 30, 
        n_ortho: int = 100, 
        rho: float = 0.95,  
        T: int = 1, 
        quantification=True,
        with_alpha_line_search: bool = True, 
        with_distance_line_search: bool = False, 
        with_interpolation: bool = False,
        bounds=(0, 1),
        *arg,
        **kwargs):
        """
        Args:
            steps (int, optional): run steps. Defaults to 1000.
            max_queries (int, optional): stop running when each example require max_queries.
            BS_gamma ([type], optional): Binary Search Early Stop. Stop when precision is below BS_gamma. Defaults to 0.01.
            BS_max_iteration ([type], optional): Max iteration for . Defaults to 10.
            theta_max (int, optional): max theta watched to evaluate the direction. Defaults to 30.
            evolution (str, optional): Move in this direction. It can be linear or circular. Defaults to "circular".
            n_ortho (int, optional): Orthogonalize with the n last directions. Defaults to 100.
            rho (float, optional): Malus/Bonus factor given to the theta_max. Defaults to 0.98.
            T (int, optional): How many evaluation done to evaluated a direction. Defaults to 1.
            with_alpha_line_search (bool, optional): Activate Binary Search on Theta. Defaults to True.
            with_distance_line_search (bool, optional): Activate Binary Search between adversarial and x_o. Defaults to False.
            with_interpolation (bool, optional): Activate Interpolation. Defaults to False.
        """
        # Attack Parameters
        self._BS_gamma = BS_gamma

        self._BS_max_iteration = BS_max_iteration

        self._steps = steps
        self._max_queries = max_queries
        self.best_advs = None
        self._theta_max = theta_max
        self.rho = rho
        self.T = T
        self.bounds = bounds
        assert self.bounds in [(0, 1), (0, 255)]
        assert self.rho <= 1 and self.rho > 0

        # Add or remove some parts of the attack
        self.with_alpha_line_search = with_alpha_line_search
        self.with_distance_line_search = with_distance_line_search
        self.with_interpolation = with_interpolation
        self.with_quantification = quantification
        if self.with_interpolation and not self.with_distance_line_search:
            Warning("It's higly recommended to use Interpolation with distance line search.")

        # Data saved during attack
        self.n_ortho = n_ortho
        self._directions_ortho: Dict[int, ep.Tensor] = {}
        self._nqueries: Dict[int, int] = {}
        self._history: Dict[int, List[float]] = {}
        self._images_finished = None

    def _quantify(self, x):
        if self.bounds == (0, 1):
            x = (x * 255 + 0.5).astype(int)
            return x / 255
        else:
            return x.astype(int) * 1.

    def get_nqueries(self) -> Dict:
        return {i: n for i, n in enumerate(self._nqueries)}

    def run(
        self,
        model: Model,
        inputs: T,
        criterion: Union[Criterion, T],
        epsilons=None,
        *,
        early_stop: Optional[float] = None,
        starting_points: Optional[ep.Tensor] = None,
        **kwargs: Any,
    ) -> T:
        originals, restore_type = ep.astensor_(inputs)

        self._nqueries = ep.zeros(originals, len(originals)).raw
        self._history  = {i: [] for i in range(len(originals))}
        self._set_cos_sin_function(originals)
        self.theta_max = ep.ones(originals, len(originals)) * self._theta_max
        self._criterion = get_criterion(criterion)
        self._criterion_is_adversarial = get_is_adversarial(criterion, model)
        self._model = model

        # Get Starting Point
        if starting_points is not None:
            self.best_advs = starting_points
        elif starting_points is None:
            init_attack: MinimizationAttack = LinearSearchBlendedUniformNoiseAttack(directions=1000, steps=0)
            self.best_advs = init_attack.run(model, originals, criterion, early_stop=early_stop)
        else:
            raise ValueError("starting_points {} doesn't exist.".format(starting_points))
        self._originals = originals

        # Check if inputs are already adversarials.
        self._images_finished = None
        self._images_finished = self._is_adversarial(originals)

        self.best_advs = ep.where(
            atleast_kd(self._images_finished, self.best_advs.ndim),
            originals,
            self.best_advs
        )
        assert self._is_adversarial(self.best_advs).all()

        self.best_advs = self._binary_search(self.best_advs, boost=True)

        # Initialize the direction orthogonalized with the first direction
        fd = self.best_advs - self._originals
        norm = ep.norms.l2(fd.flatten(1), axis=1)
        fd = fd / atleast_kd(norm, fd.ndim)
        self._directions_ortho = {i: v.expand_dims(0) for i, v in enumerate(fd)}

        # Load Basis
        if "basis_params" in kwargs:
            self._basis = Basis(self._originals, **kwargs["basis_params"])
        else:
            self._basis = Basis(self._originals)

        for step in range(self._steps):
            # Get candidates. Shape: (n_candidates, batch_size, image_size)
            candidates = self._get_candidates()
            candidates = candidates.transpose((1, 0, 2, 3, 4))
            
            best_candidates = ep.zeros_like(self.best_advs).raw
            for i, o in enumerate(self._originals):
                o_repeated = ep.concatenate([o.expand_dims(0)] * len(candidates[i]), axis=0)
                index = ep.argmax(self.distance(o_repeated, candidates[i])).raw
                best_candidates[i] = candidates[i][index].raw

            is_success = self.distance(best_candidates, self._originals) < self.distance(self.best_advs, self._originals)
            self.best_advs = ep.where(
                atleast_kd(is_success * self._images_finished.logical_not(), best_candidates.ndim), 
                ep.astensor(best_candidates), 
                self.best_advs
                )
            
            if self._images_finished.all():
                print("Max queries attained for all the images.")
                break
        return self.best_advs

    def _is_adversarial(self, perturbed: ep.Tensor) -> ep.Tensor:
        # Faster than true_is_adversarial in batch  (time gain 20% )
        # Count the queries made for each image
        # Count if the vector is different from the null vector
        labels_truth = self._criterion.labels
        is_advs = torch.BoolTensor([True] * len(labels_truth))
        if hasattr(perturbed.raw, "is_cuda") and perturbed.raw.is_cuda:
            is_advs = is_advs.cuda(perturbed.raw.device)
        indexes = []
        distances = self.distance(self._originals, perturbed)
        for i, p in enumerate(perturbed):
            if not (p == 0).all():
                self._nqueries[i] += 1
                indexes.append(i)

        if len(indexes) != 0:
            prediction = self._model(perturbed[indexes]).argmax(1)
            for i, ind in enumerate(indexes):
                is_advs[ind] = (labels_truth[ind] != prediction[i]).raw
                self._history[ind].append((float(distances[ind].raw.cpu()), bool(is_advs[ind])))

        if self._images_finished is not None:
            self._images_finished = ep.logical_or(self._images_finished, ep.astensor(self._nqueries) > self._max_queries)
        else:
            self._images_finished = ep.astensor(self._nqueries) > self._max_queries

        return ep.astensor(is_advs)
    """
    def _is_adversarial(self, perturbed: ep.Tensor) -> ep.Tensor:
        # Count the queries made for each image
        # Count if the vector is different from the null vector
        is_advs = self._criterion_is_adversarial(perturbed)
        distances = self.distance(self._originals, perturbed)
        for i, p in enumerate(perturbed):
            if not (p == 0).all():
                self._nqueries[i] += 1
                self._history[i].append((float(distances[i].raw.cpu()), bool(is_advs[i].raw)))
        if self._images_finished is not None:
            self._images_finished = ep.logical_or(self._images_finished, ep.astensor(self._nqueries) > self._max_queries)
        else:
            self._images_finished = ep.astensor(self._nqueries) > self._max_queries

        return is_advs
    """

    def _get_candidates(self) -> ep.Tensor:
        """
        Find the lowest epsilon to misclassified x following the direction: q of class 1 / q + eps*direction of class 0
        """
        epsilons = ep.where(self._images_finished, ep.ones(self._originals, len(self._originals)), ep.zeros(self._originals, len(self._originals)))
        direction_2 = ep.zeros_like(self._originals)
        while (epsilons == 0).any():
            epsilons = ep.where(self._images_finished, ep.ones_like(epsilons), epsilons)

            new_directions = self._basis.get_vector(self._directions_ortho, indexes = [i for i, eps in enumerate(epsilons) if i == 0])
            direction_2 = ep.where(
                atleast_kd(epsilons == 0, direction_2.ndim),
                new_directions,
                direction_2
            )
            for i, eps_i in enumerate(epsilons):
                if eps_i == 0:
                    # Concatenate the first directions and the last directions generated
                    self._directions_ortho[i] = ep.concatenate((
                        self._directions_ortho[i][:1],
                        self._directions_ortho[i][1 + len(self._directions_ortho[i]) - self.n_ortho:], 
                        direction_2[i].expand_dims(0)), axis=0)
            
            function_evolution = self._get_evolution_function(direction_2)
            
            new_epsilons = self._get_best_theta(function_evolution, epsilons == 0)

            self.theta_max = ep.where(new_epsilons == 0, self.theta_max * self.rho, self.theta_max)
            self.theta_max = ep.where((new_epsilons != 0) * (epsilons == 0), self.theta_max / self.rho, self.theta_max)
            epsilons = ep.where((new_epsilons != 0) * (epsilons == 0), new_epsilons, epsilons)

        function_evolution = self._get_evolution_function(direction_2)
        if self.with_alpha_line_search:
            epsilons = self._alpha_binary_search(function_evolution, epsilons)

        epsilons = epsilons.expand_dims(0)
        if self.with_interpolation:
            epsilons =  ep.concatenate((epsilons, epsilons / 2), axis=0)

        candidates = ep.concatenate([function_evolution(eps).expand_dims(0) for eps in epsilons], axis=0)
        
        if self.with_interpolation:
            d = self.distance(self.best_advs, self._originals)
            delta = self.distance(self._binary_search(candidates[1],  boost=True), self._originals)
            theta_star = epsilons[0]

            num = theta_star * (4 * delta - d * (self._cos(theta_star.raw) + 3))
            den = 4 * (2 * delta - d * (self._cos(theta_star.raw) + 1))

            theta_hat = num / den
            q_interp = function_evolution(theta_hat)
            if self.with_distance_line_search:
                q_interp = self._binary_search(q_interp,  boost=True)
            candidates = ep.concatenate((candidates, q_interp.expand_dims(0)), axis=0)

        return candidates

    def _get_evolution_function(self, direction_2: ep.Tensor) -> Callable[[ep.Tensor], ep.Tensor]:
        distances = self.distance(self.best_advs, self._originals)
        direction_1 = (self.best_advs - self._originals).flatten(start=1) / distances.reshape((-1, 1))
        direction_1 = direction_1.reshape(self._originals.shape)
        if self.with_quantification:
            return lambda theta: self._quantify((self._originals + self._add_step_in_circular_direction(direction_1, direction_2, distances, theta)).clip(*self.bounds))
        else:
            return lambda theta: (self._originals + self._add_step_in_circular_direction(direction_1, direction_2, distances, theta)).clip(*self.bounds)

        
    def _add_step_in_circular_direction(self, direction1: ep.Tensor, direction2: ep.Tensor, r: ep.Tensor, degree: ep.Tensor) -> ep.Tensor:
        degree = atleast_kd(degree, direction1.ndim).raw
        r = atleast_kd(r, direction1.ndim)
        results = self._cos(degree * np.pi/180) * direction1 + self._sin(degree * np.pi/180) * direction2
        results = results * r * self._cos(degree * np.pi / 180)
        return ep.astensor(results)

    def _get_best_theta(
            self, 
            function_evolution: Callable[[ep.Tensor], ep.Tensor], 
            mask: ep.Tensor) -> ep.Tensor:

        coefficients = ep.zeros(self._originals, 2 * self.T).raw
        for i in range(0, self.T):
            coefficients[2* i] = 1 - (i / self.T)
            coefficients[2 * i + 1] = - coefficients[2* i]

        best_params = ep.zeros_like(self.theta_max)
        for i,  coeff in enumerate(coefficients):
            params = coeff * self.theta_max
            x_evol = function_evolution(params)
            x = ep.where(
                atleast_kd((best_params == 0) * mask, self._originals.ndim), 
                x_evol, 
                ep.zeros_like(self._originals))

            is_advs = self._is_adversarial(x)

            best_params = ep.where(
                (best_params == 0) * mask * is_advs,
                params,
                best_params
            )
            if (best_params != 0).all():
                break
        
        return best_params       

    def _alpha_binary_search(
            self, 
            function_evolution: Callable[[ep.Tensor], ep.Tensor], 
            lower: ep.Tensor) -> ep.Tensor:    
        # Upper --> not adversarial /  Lower --> adversarial

        mask = self._images_finished.logical_not()
        def get_alpha(theta: ep.Tensor) -> ep.Tensor:
            return 1 - ep.astensor(self._cos(theta.raw * np.pi / 180))

        lower = ep.where(mask, lower, ep.zeros_like(lower))
        check_opposite = lower > 0 # if param < 0: abs(param) doesn't work
        
        # Get the upper range
        upper = ep.where(
            ep.logical_and(abs(lower) != self.theta_max, mask), 
            lower + ep.sign(lower) * self.theta_max / self.T,
            ep.zeros_like(lower)
            )

        mask_upper = ep.logical_and(upper == 0, mask)
        while mask_upper.any():
            # Find the correct lower/upper range
            # if True in mask_upper, the range haven't been found
            new_upper = lower + ep.sign(lower) * self.theta_max / self.T
            x = ep.where(
                atleast_kd(mask_upper, self._originals.ndim),
                function_evolution(new_upper),
                ep.zeros_like(self._originals)
            )

            is_advs =  self._is_adversarial(x)
            lower = ep.where(ep.logical_and(mask_upper, is_advs), new_upper, lower) 
            upper = ep.where(ep.logical_and(mask_upper, is_advs.logical_not()), new_upper, upper) 
            mask_upper = mask_upper * is_advs

        step = 0
        over_gamma = abs(get_alpha(upper) - get_alpha(lower)) > self._BS_gamma
        while step < self._BS_max_iteration and over_gamma.any(): 
            mid_bound = (upper + lower) / 2
            mid = ep.where(
                atleast_kd(ep.logical_and(mid_bound != 0, over_gamma), self._originals.ndim),
                function_evolution(mid_bound),
                ep.zeros_like(self._originals)
            )
            is_adv = self._is_adversarial(mid)

            mid_opp = ep.where(
                atleast_kd(ep.logical_and(ep.astensor(check_opposite), over_gamma), mid.ndim),
                function_evolution(-mid_bound),
                ep.zeros_like(mid)
            )
            is_adv_opp = self._is_adversarial(mid_opp)

            lower = ep.where(mask * over_gamma * is_adv, mid_bound, lower)
            lower = ep.where(mask * over_gamma * is_adv.logical_not() * check_opposite * is_adv_opp, -mid_bound, lower)
            upper = ep.where(mask * over_gamma * is_adv.logical_not() * check_opposite * is_adv_opp, - upper, upper)
            upper = ep.where(mask * over_gamma * (abs(lower) != abs(mid_bound)), mid_bound, upper)

            check_opposite = mask * over_gamma * check_opposite * is_adv_opp * (lower > 0)
            over_gamma = abs(get_alpha(upper) - get_alpha(lower)) > self._BS_gamma

            step += 1
        #print("Part 3: {:.4f}".format(time.time() - start_time))
        
        return ep.astensor(lower)

    def _binary_search(self, perturbed: ep.Tensor, boost: Optional[bool] = False) -> ep.Tensor:
        # Choose upper thresholds in binary search based on constraint.
        highs = ep.ones(perturbed, len(perturbed))
        d = np.prod(perturbed.shape[1:])
        thresholds = self._BS_gamma / (d * math.sqrt(d))
        lows = ep.zeros_like(highs)
        mask = atleast_kd(self._images_finished, perturbed.ndim)

        # Boost Binary search
        if boost:
            boost_vec = ep.where(mask, ep.zeros_like(perturbed), 0.1 * self._originals + 0.9 * perturbed)
            is_advs = self._is_adversarial(boost_vec)
            is_advs = atleast_kd(is_advs, self._originals.ndim)
            originals = ep.where(is_advs.logical_not(), boost_vec, self._originals)
            perturbed = ep.where(is_advs, boost_vec, perturbed)
        else:
            originals = self._originals

        # use this variable to check when mids stays constant and the BS has converged
        iteration = 0
        while ep.any(highs - lows > thresholds) and iteration < self._BS_max_iteration:
            iteration += 1
            mids = (lows + highs) / 2
            epsilons = atleast_kd(mids, originals.ndim)

            mids_perturbed = ep.where(
                mask, 
                ep.zeros_like(perturbed), 
                (1.0 - epsilons) * originals + epsilons * perturbed)

            is_adversarial_ = self._is_adversarial(mids_perturbed)

            highs = ep.where(is_adversarial_, mids, highs)
            lows = ep.where(is_adversarial_, lows, mids)

        epsilons = atleast_kd(highs, originals.ndim)
        return ep.where(mask, perturbed, (1.0 - epsilons) * originals + epsilons * perturbed)

    def _set_cos_sin_function(self, v: ep.Tensor) -> None:
        if isinstance(v.raw, torch.Tensor):
            self._cos, self._sin = torch.cos, torch.sin
        elif isinstance(v.raw, np.array):
            self._cos, self._sin = np.cos, np.sin
        else:
            raise ValueError("Cos and sin functions, not available for this instances.")


class Basis:
    def __init__(self, originals: ep.Tensor, random_noise: str = "normal", basis_type: str = "dct", **kwargs : Any):
        """
        Args:
            random_noise (str, optional): When basis is created, a noise will be added.This noise can be normal or 
                                          uniform. Defaults to "normal".
            basis_type (str, optional): Type of the basis: DCT, Random, Genetic,. Defaults to "random".
            device (int, optional): [description]. Defaults to -1.
            args, kwargs: In args and kwargs, there is the basis params:
                    * Random: No parameters                    
                    * DCT:
                            * function (tanh / constant / linear): function applied on the dct
                            * alpha
                            * beta
                            * lambda
                            * frequence_range: integers or float
                            * min_dct_value
                            * dct_type: 8x8 or full
        """
        self._originals = originals
        self._direction_shape = originals.shape[1:]
        self.basis_type = basis_type

        self._load_params(**kwargs)

        assert random_noise in ["normal", "uniform"]
        self.random_noise = random_noise

    def get_vector(self, ortho_with: Optional[Dict] = None, bounds: Tuple[float, float] = (0, 1), indexes: Optional[List[int]] = None) -> ep.Tensor:
        if ortho_with is None:
            ortho_with = {i: None for i in range(len(self._originals))}
        if indexes is None:
            indexes = range(len(self._originals))
        vectors = [
            self.get_vector_i(i, ortho_with[i], bounds).expand_dims(0)
            for i in indexes
        ]
        return ep.concatenate(vectors, axis=0)
    
    def get_vector_i(self, index: int, ortho_with: Optional[ep.Tensor] = None, bounds: Tuple[float, float] = (0, 1)) -> ep.Tensor:
        r: ep.Tensor = getattr(self, "_get_vector_i_" + self.basis_type)(index, bounds)
        if ortho_with is not None:
            r_repeated = ep.concatenate([r.expand_dims(0)] * len(ortho_with), axis=0)
            
            #inner product
            gs_coeff = (ortho_with * r_repeated).flatten(1).sum(1)
            proj = atleast_kd(gs_coeff, ortho_with.ndim) * ortho_with
            r = r - proj.sum(0)
        return r / ep.norms.l2(r)

    def _get_vector_i_dct(self, index: int, bounds: Tuple[float, float]) -> ep.Tensor:
        probs = np.random.randint(-2, 1, self.dcts[index].shape) + 1
        r_np = self.dcts[index] * probs
        r_np = ep.from_numpy(self._originals, self._inverse_dct(r_np).astype("float32"))

        return r_np + ep.normal(self._originals, r_np.shape, stddev=self._beta)

    def _get_vector_i_random(self, index: int, bounds: Tuple[float, float]) -> ep.Tensor:
        r = ep.zeros(self._originals, self._direction_shape)
        r = getattr(ep, self.random_noise)(r, r.shape, *bounds)
        return ep.astensor(r)

    def _load_params(
            self, 
            beta: float = 0,
            frequence_range: Tuple[float, float] = (0, 0.5),
            dct_type: str = "8x8",
            function: str = "tanh",
            gamma: float = 1,
            energetic_gaussian: bool = True
            ) -> None:
        if not hasattr(self, "_get_vector_i_" + self.basis_type):
            raise ValueError("Basis {} doesn't exist.".format(self.basis_type))

        if self.basis_type == "dct":
            self._beta = beta
            if dct_type == "8x8":
                mask_size = (8, 8) 
                dct_function = lambda x, mask: dct2_8_8(x, mask, energetic_gaussian)
                self._inverse_dct = idct2_8_8
            elif dct_type == "full":
                mask_size = (self._direction_shape[-2], self._direction_shape[-1])
                dct_function = dct2_full
                self._inverse_dct = idct2
            else:
                raise ValueError("DCT {} doesn't exist.".format(dct_type))
            
            dct_mask = get_zig_zag_mask(frequence_range, mask_size)
            self.dcts = np.array([dct_function(np.array(image.raw.cpu()), dct_mask) for image in self._originals])
        
            def get_function(function: str, gamma: float) -> Callable:
                if function == "tanh":
                    return lambda x: np.tanh(gamma * x)
                elif function == "identity":
                    return lambda x: x
                elif function == "constant":
                    return lambda x: (abs(x) > 0).astype(int)
                else:
                    raise ValueError("Function given for DCT is incorrect.")

            self.dcts = get_function(function, gamma)(self.dcts)


###########################
# DCT Functions
###########################


def dct2(a: Any) -> Any:
    return fft.dct(fft.dct(a, axis=1, norm='ortho' ), axis=2, norm='ortho')

def idct2(a: Any) -> Any:
    return fft.idct(fft.idct(a, axis=1, norm='ortho'), axis=2, norm='ortho')

def dct2_8_8(image: Any, mask: Any=None, energetic_gaussian: bool = False) -> Any:
    if mask is None:
        mask = np.ones((8, 8))
    if mask.shape != (8, 8):
        raise ValueError("Mask have to be with a size of (8, 8)")

    imsize = image.shape
    dct = np.zeros(imsize)
    n_channel = imsize[0]
    mask = mask.reshape((1, 8, 8)).repeat(n_channel, 0)
    energy_by_block = np.zeros((int(imsize[1] / 8), int(imsize[2] / 8)))
    for i in np.r_[:imsize[1]:8]:
        for j in np.r_[:imsize[2]:8]:
            dct_i_j = dct2(image[:, i:(i+8),j:(j+8)]) 
            dct[:, i:(i+8),j:(j+8)] = dct_i_j * mask[:dct_i_j.shape[0], :dct_i_j.shape[1]]
            energy_by_block[int(i / 8), int(j / 8)] = sum(abs(dct[:, i:(i+8),j:(j+8)].flatten()))

    if energetic_gaussian:
        y = list(energy_by_block.flatten())
        mean = (max(y) + min(y)) / 2 #  np.mean(x)
        std = np.std(y)
        energy_function = lambda x: np.exp(- (x - mean) ** 2 / (2 * std ** 2)) / (np.sqrt(2 * np.pi) * std)
        dct_energy = np.linalg.norm(dct.flatten())
        for i in np.r_[:imsize[1]:8]:
            for j in np.r_[:imsize[2]:8]:
                energy = np.sum(abs(dct[:, i:(i+8),j:(j+8)].flatten()))
                if energy > 0:
                  dct[:, i:(i+8),j:(j+8)] *= energy_function(energy) / energy
        dct *= dct_energy / np.linalg.norm(dct.flatten())
    return dct

def idct2_8_8(dct: Any) -> Any:
    im_dct = np.zeros(dct.shape)
    for i in np.r_[:dct.shape[1]:8]:
        for j in np.r_[:dct.shape[2]:8]:
            im_dct[:, i:(i+8),j:(j+8)] = idct2(dct[:, i:(i+8),j:(j+8)])
    return im_dct

def dct2_full(image: Any, mask: Any = None) -> Any:
    if mask is None:
        mask = np.ones(image.shape[-2:])

    imsize = image.shape
    mask = mask.reshape((1, imsize[1], imsize[2])).repeat(imsize[0], 0)
    dct = dct2(image) * mask
    return dct

def get_zig_zag_mask(frequence_range: Tuple[float, float], mask_shape: Tuple[int, int] = (8, 8)) -> Any:
    mask = np.zeros(mask_shape)
    s = 0
    total_component = sum(mask.flatten().shape)
    
    if frequence_range[1] <= 1:
        n_coeff = int(total_component * frequence_range[1])
    else:
        n_coeff = int(frequence_range[1])

    if frequence_range[0] <= 1:
        min_coeff = int(total_component * frequence_range[0])
    else:
        min_coeff = int(frequence_range[0])
    
    while n_coeff > 0:
        for i in range(min(s + 1, mask_shape[0])):
            for j in range(min(s + 1, mask_shape[1])):
                if i + j == s:
                    if min_coeff > 0:
                        min_coeff -= 1
                        continue

                    if s % 2:
                        mask[i, j] = 1
                    else:
                        mask[j, i] = 1
                    n_coeff -= 1
                    if n_coeff == 0:
                        return mask
        s += 1
    return mask
