config_wb =  {
        "steps": 80,
        "gamma": 0.3,
        "quantification": True
    }

config_bb =  {
        "steps": 50000, 
        "max_queries": 5000,
        "BS_gamma": 0.05, 
        "BS_max_iteration": 7, 
        "theta_max": 30, 
        "n_ortho": 100, 
        "rho": 0.95,  
        "T": 1,
        "quantification": True,
        "basis_params": {
            "random_noise": "normal",
            "basis_type": "dct",
            "beta": 0.01,
            "frequence_range": [0, 0.5],
            "dct_type": "full",
            "function": "tanh",
            "gamma": 1,
            "energetic_gaussian": False
        }
}