import os
import math
import torch
import numpy as np
import matplotlib.pyplot as plt
import itertools
import pickle

from foolbox.attacks.base import get_criterion
from foolbox.models import PyTorchModel

from robic.boundary_projection import BP
from robic.surfree import SurFree
from robic.config import config_wb, config_bb


class RoBIC:
    def __init__(self, output_folder=None, device=None):
        self.output_folder = output_folder

        config_bb["device"] = device
        config_wb["device"] = device
        self._wb = BP(**config_wb)
        self._bb = SurFree(**config_bb)

    def run(self, model, X, y, batch_size=16, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], save=True):
        if save and self.output_folder is None:
            raise ValueError("If you want to save, an output folder is needed.")

        preprocessing = dict(mean=mean, std=std, axis=-3)
        fmodel = PyTorchModel(model, bounds=(0, 1), preprocessing=preprocessing)
        
        # Accuracy
        n_batch = int(np.ceil(len(X) / batch_size))
        already_advs = []
        for i in range(n_batch):
            X_batch = X[i * batch_size: (i + 1) * batch_size]
            y_batch = y[i * batch_size: (i + 1) * batch_size]
            prediction = fmodel(X_batch / 255).argmax(1)
            already_advs.append((prediction != y_batch))
        already_advs = torch.cat(already_advs)
        accuracy = 1 - sum(already_advs) / len(y)
        print("Model Accuracy: {}".format(accuracy))

        # Remove Misclassified Images
        indexes_mis = [i for i, b in enumerate(already_advs) if b]
        indexes_kept = [i for i, b in enumerate(already_advs) if not b]
        X_mis, y_mis = X[indexes_mis], y[indexes_mis]
        X, y = X[indexes_kept], y[indexes_kept]
        mis_norms = self._get_norms(X_mis, X_mis)

        # Variables
        n_batch = int(np.ceil(len(X) / batch_size))
        wb_norms, bb_norms = [], []

        # WB Robustness
        for i in range(n_batch):
            print("WB - Batch: {}".format(i))
            X_batch = X[i * batch_size: (i + 1) * batch_size]
            y_batch = y[i * batch_size: (i + 1) * batch_size]
            wb_advs, _, _ = self._wb(fmodel, X_batch, get_criterion(y_batch))
            self._assert_quantified_adversarial(fmodel, wb_advs.round(), y_batch.round())
            wb_norm_i = self._get_norms(X_batch, wb_advs)
            wb_norm_i = np.where(wb_norm_i < 10e-5, np.inf, wb_norm_i)
            wb_norms.append(wb_norm_i)
        if save and self.output_folder is not None:
            filepath = os.path.join(self.output_folder, "white_box_distortion.pdf") if save else None
            with open(os.path.join(self.output_folder, "wb_results.pkl"), "wb") as f:
                pickle.dump(np.concatenate(wb_norms + [mis_norms]), f)
        else:
            filepath = None
        wb_robustness = self._get_half_distortion(np.concatenate(wb_norms + [mis_norms]), filepath)
        print("White Box Half-Distortion: {}".format(wb_robustness))

        # BB Robustness
        for i in range(n_batch):
            print("BB - Batch: {}".format(i))
            X_batch = X[i * batch_size: (i + 1) * batch_size]
            y_batch = y[i * batch_size: (i + 1) * batch_size]
            bb_advs, _, _ = self._bb(fmodel, X_batch / 255, y_batch, epsilons=None, **config_bb)
            bb_advs *= 255
            self._assert_quantified_adversarial(fmodel, bb_advs.round(), y_batch.round())
            bb_norms.append(self._get_norms(X_batch, bb_advs))


        if save and self.output_folder is not None:
            filepath = os.path.join(self.output_folder, "black_box_distortion.pdf") if save else None
            with open(os.path.join(self.output_folder, "bb_results.pkl"), "wb") as f:
                pickle.dump(np.concatenate(bb_norms + [mis_norms]), f)
        else:
            filepath = None
        bb_robustness = self._get_half_distortion(np.concatenate(bb_norms + [mis_norms]), filepath)
        print("Black Box Half-Distortion: {}".format(bb_robustness))

        return accuracy, wb_robustness, bb_robustness

    def _get_iterator_param(self, params_dict):
        keys, values = zip(*params_dict.items())
        yield len(list(itertools.product(*values)))
        for val in itertools.product(*values):
            yield {k: v for k, v in zip(keys, val)}


    def _get_half_distortion(self, distances, filepath=None):
        m = max([e for e in distances if e != np.inf])
        dists, p_success = self._get_p_success(distances, min=0, max=1.2*m)
        if filepath is not None:
            self._plot_p_success(dists, p_success, filepath)

        reg_score = self._exponential_regression(dists, p_success, steps=2000, early_stop=0.0001)
        return math.log(2) / reg_score

    def _assert_quantified_adversarial(self, model, X_adv, y):
        y_adv = model(X_adv / 255).argmax(1)
        assert all(y_adv != y)

    def _get_norms(self, X, X_advs):
        N = X.shape[1] * X.shape[2] * X.shape[3]
        norm = torch.linalg.norm((X - X_advs).float(), dim=[1, 2, 3]) / math.sqrt(N)
        return norm.cpu().numpy()

    def _get_p_success(self, distances, min=0, max=50, step=0.01):
        n = int((max - min) / step)
        y = []
        x = np.linspace(min, max, n)
        N = len(distances)
        # Remove elements that are not adversarial
        distances = distances[distances > 10e-10]
        for t in x:
            y.append(sum(distances >= t) / N)
        return x, y
    
    def _exponential_regression(self, x, y, steps=10000, early_stop=0.001):
        get_error = lambda a, b: sum((a - b) ** 2) / len(a)
        f = lambda gamma: y[0] * np.exp(- gamma * x)
        
        gamma = 1
        best_gamma = 1

        r = f(gamma)
        min_error = get_error(y, r)
            
        gamma_step = 0.5
        for i in range(steps):
            g1, g2 = (1 + gamma_step) * best_gamma, (1 - gamma_step) * best_gamma
            changed = False
            for g in [g1, g2]:
                r = f(g)
                error = get_error(y, r)
                if min_error > error:
                    min_error = error 
                    best_gamma = g
                    changed = True
                    break
            if changed == False:
                gamma_step /= 2

            if abs(min_error) < early_stop:
                print("EARLY STOP")
                break
        return best_gamma
        
    def _plot_p_success(self, dists, p_success, filepath):
        plt.plot(dists, p_success)
        plt.xlabel("Distortion")
        plt.ylabel("Accuracy")
        plt.ylim((-0.1, 1.1))
        plt.savefig(filepath)
        